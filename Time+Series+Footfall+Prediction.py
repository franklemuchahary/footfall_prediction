
# coding: utf-8

# In[9]:

import pandas as pd
import numpy as np
import re
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import seaborn as sns
from collections import Counter
from sklearn.metrics import mean_squared_error
get_ipython().magic('matplotlib inline')
from pylab import rcParams
rcParams['figure.figsize'] = 15, 10


# In[10]:

data = pd.read_csv("train.csv")
test_data = pd.read_csv("test.csv")


# In[11]:

def convert_string_to_date_time(datetime_str):
    datetime_obj = datetime.strptime(datetime_str, "%Y%m%d%H")
    return datetime_obj

def convert_string_to_time(datetime_str):
    datetime_obj = datetime.strptime(datetime_str, "%Y%m%d%H")
    return datetime_obj.time()

def convert_id_to_string(ID):
    return str(ID)


# In[12]:

data['ID'] = data['ID'].apply(convert_id_to_string)
data['Date_Time'] = data['ID'].apply(convert_string_to_date_time)


# # USE ARIMA - Prepare Dataset for ARIMA and Train USING ARIMA

# In[13]:

data = data.set_index(pd.DatetimeIndex(data['Date_Time']))


# In[14]:

tot_train_data = pd.DataFrame(data['Count'])
tot_train_data['Count'] = pd.to_numeric(tot_train_data['Count'], downcast='float')


# In[15]:

train_set = pd.DataFrame(data['Count'][:'2012'])
test_set = pd.DataFrame(data['Count']['2013':])


# In[16]:

train_set['Count'] = pd.to_numeric(train_set['Count'], downcast='float')


# In[17]:

plt.plot(test_set)


# In[18]:

from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.stattools import acf, pacf
from statsmodels.tsa.arima_model import ARIMA


# In[19]:

def test_stationarity(timeseries):
    
    #Determing rolling statistics
    rolmean = pd.rolling_mean(timeseries, window=24)
    rolstd = pd.rolling_std(timeseries, window=24)

    #Plot rolling statistics:
    orig = plt.plot(timeseries, color='blue',label='Original')
    mean = plt.plot(rolmean, color='red', label='Rolling Mean')
    std = plt.plot(rolstd, color='black', label = 'Rolling Std')
    plt.legend(loc='best')
    plt.title('Rolling Mean & Standard Deviation')
    plt.show(block=False)
    
    #Perform Dickey-Fuller test:
    print('Results of Dickey-Fuller Test:')
    dftest = adfuller(timeseries, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['Test Statistic','p-value','#Lags Used','Number of Observations Used'])
    for key,value in dftest[4].items():
        dfoutput['Critical Value (%s)'%key] = value
    print(dfoutput)


# In[20]:

test_stationarity(tot_train_data['Count'])


# In[22]:

ts_log = np.log(train_set['Count'])

ts_log2 = np.log(tot_train_data['Count'])


# In[23]:

ts_log_diff = ts_log - ts_log.shift()
ts_log_diff2 = ts_log2 - ts_log2.shift()


plt.plot(ts_log_diff)


# In[25]:

from statsmodels.tsa.seasonal import seasonal_decompose

decomposition = seasonal_decompose(ts_log)

trend = decomposition.trend
seasonal = decomposition.seasonal
residual = decomposition.resid

plt.subplot(411)
plt.plot(ts_log, label='Original')
plt.legend(loc='best')
plt.subplot(412)
plt.plot(trend, label='Trend')
plt.legend(loc='best')
plt.subplot(413)
plt.plot(seasonal,label='Seasonality')
plt.legend(loc='best')
plt.subplot(414)
plt.plot(residual, label='Residuals')
plt.legend(loc='best')
plt.tight_layout()


# In[26]:

ts_log_decompose = residual
ts_log_decompose.dropna(inplace=True)
test_stationarity(ts_log_decompose)


# In[27]:

ts_log_diff.dropna(inplace=True)
test_stationarity(ts_log_diff)


# In[1]:

lag_acf = acf(ts_log_diff, nlags=10)
lag_pacf = pacf(ts_log_diff, nlags=10, method='ols')

#Plot ACF: 
plt.subplot(121) 
plt.plot(lag_acf)
plt.axhline(y=0,linestyle='--',color='gray')
plt.axhline(y=-1.28/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
plt.axhline(y=1.28/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
plt.title('Autocorrelation Function')


#Plot PACF:
plt.subplot(122)
plt.plot(lag_pacf)
plt.axhline(y=0,linestyle='--',color='gray')
plt.axhline(y=-1.28/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
plt.axhline(y=1.28/np.sqrt(len(ts_log_diff)),linestyle='--',color='gray')
plt.title('Partial Autocorrelation Function')
plt.tight_layout()


# In[49]:

model = ARIMA(ts_log2, order=(5, 1, 4))
results_AR = model.fit(disp=1, maxiter=500, solver='lbfgs')  
plt.plot(ts_log_diff2)
plt.plot(results_AR.fittedvalues, color='red')
plt.title('RMSE: %.4f'% np.sqrt(np.sum((results_AR.fittedvalues-ts_log_diff2)**2)/len(ts_log_diff2)))


# In[44]:

sns.distplot(results_AR.resid, kde=True)


# #### Differencing Predictions - Bringing Back to Scale

# In[59]:

preds = results_AR.predict(start=len(ts_log), end=len(ts_log)+2928)
temp = [x for x in ts_log]
ans = []
for y_hat in preds:
    inv = y_hat + temp[-1]
    temp.append(inv)
    ans.append(np.exp(inv))


# In[60]:

len(ans)


# In[58]:

ans = ans[:1500]


# In[61]:

ans_rounded = np.round(ans)


# In[38]:

np.sqrt(mean_squared_error(test_set['Count'],ans_rounded[:-1]))


# ### Decomposed Predictions

# In[45]:

preds = results_AR.predict(start=len(ts_log), end=len(ts_log)+2927)


# In[47]:

results_AR.resid


# #### Iterative forecasting Process with ARIMA
# 
# Did not work as expected
def get_inverse_diff_value(array):
    inv_list = []
    for y_hat in array:
        inv = y_hat + history[-1]
        history.append(inv)
        inv_list.append(inv)
    return history, inv_list    
        
def flatten_array(nested_array):
    flat = [item for sublist in nested_array for item in sublist]
    return flat

history = [x for x in ts_log]
predictions = list()

for i in range(0, int(2928/24)):
    model = ARIMA(history, order=(3, 1, 2))  
    results_AR = model.fit(disp=1, maxiter=100)
    op = results_AR.predict(start=len(history)+1, end=len(history)+23)
    y_hat_array = op
    #print(y_hat_array)
    _,inv = get_inverse_diff_value(y_hat_array)
    predictions.append(inv)
    print(np.exp(inv))
    print("Prediction Set: ", len(predictions))len(predictions[1])preds_flat = flatten_array(predictions)preds_flat_actual_values = np.exp(preds_flat)preds_flat_rounded = np.round(preds_flat_actual_values)len(preds_flat)history = [x for x in tot_train_data['Count']]
predictions = list()

for i in range(0,20):
    model = ARIMA(history, order=(1, 0, 2))  
    results_AR = model.fit(disp=1)
    op = results_AR.forecast()
    y_hat = op[0][0]
    predictions.append(y_hat)
    history.append(y_hat)
    print("...Data Point: ", i)
# # Converting TS Data to normal data

# In[120]:

lin_data = data[['Count', 'Date_Time']]


# In[121]:

lin_data.to_csv('ts_data.csv', sep=',', index=False)


# In[82]:

def get_year_from_date(date):
    return date.year

def get_time_from_date(date):
    return date.time().hour

def get_weekday_from_date(date):
    return date.weekday()

def get_month_from_date(date):
    return date.month

def get_date_from_date_time(date_time):
    return date_time.date()


# In[83]:

lin_data['Year'] = lin_data['Date_Time'].apply(get_year_from_date)
lin_data['Month'] = lin_data['Date_Time'].apply(get_month_from_date)
lin_data['Weekday'] = lin_data['Date_Time'].apply(get_weekday_from_date)
lin_data['Hour'] = lin_data['Date_Time'].apply(get_time_from_date)
lin_data['Date'] = lin_data['Date_Time'].apply(get_date_from_date_time)


# In[90]:

prev_week_count = lin_data['Count'] 
lin_data_new = lin_data[24*7:]
lin_data_new = lin_data_new.reset_index(drop=True)
lin_data_new['Prev_week_count'] = prev_week_count[:-24*7]


# In[93]:

mean_values = lin_data_new.groupby('Date', as_index=False).mean()[['Count', 'Date']]
temp = pd.merge(lin_data_new, mean_values, how='inner', left_on='Date', right_on='Date', 
                suffixes=('_actual_value','_daily_mean'))
lin_data_new = temp


# In[100]:

lin_data_new.columns


# In[102]:

X = lin_data_new[['Year','Month','Weekday','Hour']]
Y = pd.DataFrame(lin_data_new['Count_actual_value'])


# In[111]:

train_x = X[:10824]
test_x = X[10824:]
train_y = Y[:10824]
test_y = Y[10824:]


# In[112]:

from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error

std_scaler = StandardScaler()
train_x_scaled = std_scaler.fit_transform(train_x)
test_x_scaled = std_scaler.fit_transform(test_x)
train_y_scaled = std_scaler.fit_transform(train_y)


# In[106]:

import math
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Embedding
from keras.layers import LSTM
import tensorflow


# In[114]:

model = Sequential()
model.add(Dense(10, input_dim=4))
model.add(Activation('relu'))
model.add(Dense(10, input_dim=10))
model.add(Activation('relu'))
model.add(Dense(1, input_dim=10))
model.add(Activation('linear'))
model.compile(loss='mean_squared_error', optimizer='adam')
model.fit(train_x_scaled, train_y_scaled, epochs=30, batch_size=1, verbose=2)


# In[115]:

mlp_preds = model.predict(test_x_scaled)
mlp_preds = std_scaler.inverse_transform(mlp_preds)
np.sqrt(mean_squared_error(mlp_preds, test_y['Count_actual_value']))


# ## Preparing for submission

# In[62]:

test_data['Count'] = ans_rounded


# In[63]:

test_data.to_csv('faltoo_submission12.csv', sep=',', index=False)


# In[64]:

pd.read_csv('faltoo_submission12.csv')


# ## Plotting
#import matplotlib.pyplot as plt

temp = pd.DataFrame({
    'preds':predictions,
    'true':xyz[0:20]
})

temp = temp.reset_index(drop=True)



plt.plot(temp['preds'], 'r--', label="Predicted")
plt.plot(temp['true'], 'g-', label="True")
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.figure(figsize=(20,10))
plt.show()